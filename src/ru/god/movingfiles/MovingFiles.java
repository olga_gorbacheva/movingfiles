package ru.god.movingfiles;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, предназначенный для перемещения файлов из одного каталога в другой
 *
 * @author Горбачева, 16ИТ18к
 */
public class MovingFiles {
    private static final String MOVE_SUCCESS = "Перемещение файла %s прошло успешно%n";
    private static final String MOVING_LIST = "Количество перемещенных файлов: %d%nСписок:%n";
    private static final String SOURCE = "Введите каталог, содержимое которого надо переместить";
    private static final String TARGET = "Введите каталог, в который будем перемещать";
    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<String> movingList = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println(SOURCE);
        String source = scanner.nextLine();
        System.out.println(TARGET);
        String target = scanner.nextLine();
        moveFiles(source, target);
        printResults();
    }

    /**
     * Метод, выполняющий перемещение файлов из одного каталога в другой
     *
     * @param source - откуда перемещаем
     * @param target - куда перемещаем
     */
    private static void moveFiles(String source, String target) {
        try (DirectoryStream<Path> directorySource = Files.newDirectoryStream(Paths.get(source))) {
            for (Path entry : directorySource) {
                Files.move(entry, Paths.get(target + entry.getFileName()),
                        StandardCopyOption.REPLACE_EXISTING);
                System.out.printf(MOVE_SUCCESS, String.valueOf(entry));
                movingList.add(String.valueOf(entry));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, выводящий информацию о перемещении файлов
     */
    private static void printResults() {
        System.out.printf(MOVING_LIST, movingList.size());
        for (String file : movingList) {
            System.out.println(file);
        }
    }
}
